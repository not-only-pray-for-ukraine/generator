const fs = require("fs");
const { createCanvas, loadImage } = require("canvas");

const imageFormat = {
    width: 1500,
    height: 1500,
};

const subcollections = [
    'child-of-war',
    'ghost-of-kyiv',
    'president-of-liberty',
];

const dir = {
    attributes : `./layers`,
    outputs: `./outputs`,
};

const numberPerSubcollection = 3333;
const totalOutputs = numberPerSubcollection * subcollections.length;

const canvas = createCanvas(imageFormat.width, imageFormat.height);
const ctx = canvas.getContext("2d");

const priorities = [
    'background',
    'text',
    'base',
    'clothing',
    'lips',
    'eyes',
    'hair',
    'head',
];

const main = async () => {
    let combinations = [];
    for (const subcollection of subcollections) {
        const attributesDir = `${dir.attributes}/${subcollection}`;
        // register all the attributes
        const types = fs.readdirSync(attributesDir);
        // set all priotized layers to be drawn first. for eg: punk type, top... You can set these values in the priorities array in line 21
        const traitTypes = priorities.concat(types.filter(x => !priorities.includes(x)))
            .filter(traitType => fs.existsSync(`${attributesDir}/${traitType}/`))
            .map(traitType => (
                fs.readdirSync(`${attributesDir}/${traitType}/`)
                    .map(value => {
                        return {
                            trait_type: traitType,
                            value: value,
                            rarity_percentage: value.substring(value.lastIndexOf(' ') + 1, value.lastIndexOf('.')),
                            subcollection: subcollection,
                        }
                    })
            ));

        const subcollectionCombinations = getCombinations(traitTypes, numberPerSubcollection)
        combinations.push(...subcollectionCombinations);
    }

    // shuffle subcollections
    combinations = shuffle(combinations, 10);

    for (var n = 0; n < totalOutputs; n++) {
        await generate(combinations[n], n);
    }
};

const recreateOutputsDir = () => {
    if (fs.existsSync(dir.outputs)) {
        fs.rmdirSync(dir.outputs, { recursive: true });
    }
    fs.mkdirSync(dir.outputs);
    fs.mkdirSync(`${dir.outputs}/metadata`);
    fs.mkdirSync(`${dir.outputs}/images`);
};

const shuffle = (array, repeat) => {
    for (let i = 0; i < repeat; i++) {
        let currentIndex = array.length, randomIndex;
        while (currentIndex != 0) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;
            [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
        }
    }
    return array;
};

const getCombinations = (traitTypes, number) => {
    const combinations = [];
    while (combinations.length < number) {
        // getting right rarity percentage and shuffle
        let attributes = {};
        for (let x = 0; x < traitTypes.length; x++) {
            let traitType = traitTypes[x];
            let traitName = traitType[0].trait_type;
            attributes[traitName] = [];
            traitType.map(attribute => {
                for (let y = 0; y < attribute.rarity_percentage; y++) {
                    attributes[traitName].push(attribute);
                }
            });
            attributes[traitName] = shuffle(attributes[traitName], 10)
        }

        // randomly combine
        for (let x = 0; x < 100; x++) {
            let combination = [];
            for (const [traitType, traits] of Object.entries(attributes)) {
                const randomIndex = Math.floor(Math.random() * traits.length);
                const trait = traits[randomIndex];
                if (trait) {
                    combination.push(traits[randomIndex]);
                    traits.splice(randomIndex, 1);
                }
            }
            // check for uniqueness
            let unique = true;
            combinations.map(comb => {
                if (JSON.stringify(combination) === JSON.stringify(comb)) {
                    unique = false;
                }
            });
            if (unique) {
                combinations.push(combination);
            }
        }
    }

    return combinations.splice(0, number);
};

const generate = async (combination, index) => {
    const drawableAttributes = combination;
    const subcollection = drawableAttributes[0].subcollection;
    const subcollectionName = subcollection.split('-').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');

    for (let index = 0; index < drawableAttributes.length; index++) {
        const attribute = drawableAttributes[index];
        const image = await loadImage(`${dir.attributes}/${subcollection}/${attribute.trait_type}/${attribute.value}`);
        ctx.drawImage(image,0,0, imageFormat.width, imageFormat.height);
    }

    console.log(`Progress: ${index+1}/${totalOutputs}`);

    //deep copy
    let metaDrawableAttributes = JSON.parse(JSON.stringify(drawableAttributes));

    //prepare attributes names
    metaDrawableAttributes.map(attribute => {
        attribute.trait_type = attribute.trait_type.charAt(0).toUpperCase() + attribute.trait_type.slice(1);
        attribute.value = attribute.value.substring(0, attribute.value.lastIndexOf(' ')).trim();
        delete attribute.rarity_percentage;
        delete attribute.subcollection;
        return attribute;
    });

    // save metadata
    fs.writeFileSync(
        `${dir.outputs}/metadata/${index+1}.json`,
        JSON.stringify({
            name: `${subcollectionName} #${index+1}`,
            description: 'Charitable project to help Ukraine. 80% of the money from the sale of each NFT (and further from royalties) is converted into BTC and transferred to the wallet of the Ukrainian Army, after the end of the war to restore the country and help the victims. Screenshots of transactions will be published on our social networks, links in bio. 20% will be used to create the next collections to help Ukraine. NFTs have a huge range of prices so that everyone can contribute according to their opportunity. It\'s time to act, not just pray.',
            image: `ipfs://IPFS_ID/${index+1}.png`,
            attributes: metaDrawableAttributes
        }, null, 2),
        function(err){
            if(err) throw err;
        }
    );

    // save image
    fs.writeFileSync(
        `${dir.outputs}/images/${index+1}.png`,
        canvas.toBuffer("image/png")
    );
};

//main
(() => {
    recreateOutputsDir();
    main();
})();
